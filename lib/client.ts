import * as _ from 'lodash';
import axios, { AxiosResponse, AxiosRequestConfig } from 'axios';

class EhClient {
  public domain: EhDomain = EhDomain.ehentai;

  private readonly host: string = `https://${this.domain.toString()}/`;

  private readonly api: string = `${this.host}api.php`;

  public async singin(
    username: string,
    password: string,
    recaptchaChallenge: string,
    recaptchaResponse: string,
  ) {
    return axios.request({
      url: 'https://forums.e-hentai.org/index.php',
      method: 'post',
      params: {
        act: 'Login',
        CODE: '01',
      },
      data: {
        UserName: username,
        PassWord: password,
        submit: 'Log me in',
        CookieDate: 1,
        temporary_https: 'off',
        recaptcha_challenge_field: !_.isEmpty(recaptchaChallenge) ? recaptchaChallenge : undefined,
        recaptcha_response_field: !_.isEmpty(recaptchaResponse) ? recaptchaResponse : undefined,
      },
    });
  }

  public async gallerys(
    page: number = 0,
    category: Category = null,
    keyword: string = null,
    advanceSearch: AdvanceSearch = null,
    minRating: number = -1,
    imagePath: string = null,
    useSimilarityScan: boolean = false,
    onlySearchCovers: boolean = false,
    showExpunged: boolean = false,
  ) {
    const paramsCategory = category ? {
      f_doujinshi: category.doujinshi ? 1 : 0,
      f_manga: category.manga ? 1 : 0,
      f_artistcg: category.artistcg ? 1 : 0,
      f_gamecg: category.gamecg ? 1 : 0,
      f_western: category.western ? 1 : 0,
      'f_non-h': category.non_h ? 1 : 0,
      f_imageset: category.imageset ? 1 : 0,
      f_cosplay: category.cosplay ? 1 : 0,
      f_asianporn: category.asianporn ? 1 : 0,
      f_misc: category.misc ? 1 : 0,
    } : null;
    const paramsKeyword = keyword ? {
      f_search: keyword,
    } : null;
    const paramsPage = page ? {
      page,
    } : null;
    const paramsAdvanceSearch = advanceSearch ? {
      advsearch: 1,
      f_sname: advanceSearch.sname ? 'on' : undefined,
      f_stags: advanceSearch.stags ? 'on' : undefined,
      f_sdesc: advanceSearch.sdesc ? 'on' : undefined,
      f_storr: advanceSearch.storr ? 'on' : undefined,
      f_sto: advanceSearch.sto ? 'on' : undefined,
      f_sdt1: advanceSearch.sdt1 ? 'on' : undefined,
      f_sdt2: advanceSearch.sdt2 ? 'on' : undefined,
      f_sh: advanceSearch.sh ? 'on' : undefined,
    } : null;
    const paramsMinRating = minRating !== -1 ? {
      f_sr: 'on',
      f_srdd: minRating,
    } : null;
    const paramsFilter = category || keyword || advanceSearch || minRating !== -1 ? {
      f_apply: 'Apply+Filter',
    } : null;

    return await axios.request({
      url: this.host,
      params: {
        ...paramsCategory,
        ...paramsKeyword,
        ...paramsPage,
        ...paramsMinRating,
      }
    });
  }

  public async gallerysByHot() {
    return await axios.request({
      url: `${this.host}`,
    });
  }

  public async gallerysByAPI(galleryInfos: GalleryInfo[]) {
    return await axios.request({
      url: this.api,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      },
      data: {
        namespace: 1,
        method: 'gdata',
        gidlist: galleryInfos.map(info => [info.gid, info.token]),
      },
    })
  }

  public async galleryDetail(
    gid: number,
    token: string,
    index: number = 0,
    allComment: boolean = false
  ) {
    return await axios.request({
      url: `${this.host}g/${gid}/${token}/`,
      params: {
        p: index !== 0 ? index : undefined,
        hc: allComment ? 1 : undefined,
      },
    });
  }

  public async preview(
    gid: number,
    token: string,
    index: number = 0,
    allComment: boolean = false
  ) {
    return await axios.request({
      url: `${this.host}g/${gid}/${token}/`,
      params: {
        p: index !== 0 ? index : undefined,
        hc: allComment ? 1 : undefined,
      },
    });
  }

  public async rateGallery(
    apiUid: number,
    apiKey: string,
    gid: number,
    token: string,
    rating: number,
  ) {
    return await axios.request({
      url: this.api,
      method: 'post',
      data: {
        gid,
        token,
        apiuid: apiUid,
        apikey: apiKey,
        method: 'rategallery',
        rating: Math.ceil(rating * 2),
      },
    });
  }

  public async commentGallery(
    gid: number,
    token: string,
    index: number = 0,
    allComment: boolean = false,
    comment: string,
  ) {
    return await axios.request({
      url: `${this.host}g/${gid}/${token}/`,
      method: 'post',
      params: {
        p: index !== 0 ? index : undefined,
        hc: allComment ? 1 : undefined,
      },
      data: {
        commenttext: comment,
        postcomment: 'Post New',
      },
    })
  }

  public async galleryToken(
    gid: number,
    gtoken: string,
    page: number,
  ) {
    return await axios.request({
      url: this.api,
      method: 'post',
      data: {
        method: 'gtoken',
        pagelist: [[ gid, gtoken, (page + 1) ]],
      },
    })
  }

  public async favorites(
    favcat: EhFavoriteCat = EhFavoriteCat.all,
    keyword: string = null,
    page: number = 0,
    callApi: boolean,
  ) {
    const paramsFavCat = (
      favcat !== EhFavoriteCat.all.valueOf()
      ||
      favcat !== EhFavoriteCat.local.valueOf()
    ) ? {
      favcat,
    } : null;
    const paramsKeyword = keyword ? {
      f_search: keyword,
      f_apply: 'Search+Favorites',
    } : null;
    const paramsPage = page > 0 ? {
      page,
    } : null;

    return await axios.request({
      url: `${this.host}favorites.php`,
      params: {
        ...paramsFavCat,
        ...paramsKeyword,
        ...paramsPage,
      },
    })
  }

  public async addFavorite(
    gid: number,
    token: string,
    favcat: EhFavoriteCat = EhFavoriteCat.storage1,
    note: string,
  ) {
    return await axios.request({
      url: `${this.host}gallerypopups.php`,
      method: 'post',
      params: {
        gid,
        token,
        act: 'addfav',
      },
      data: {
        favcat,
        favnote: note || '',
        submit: 'Apply Changes',
        update: 1,
      }
    })
  }

  public async modifyFavorites(
    gids: number[],
    favcat: EhFavoriteCat = EhFavoriteCat.storage1,
    keyword: string = null,
    page: number = 0,
    callApi: boolean,
  ) {
    const paramsFavCat = (
      favcat !== EhFavoriteCat.all.valueOf()
      ||
      favcat !== EhFavoriteCat.local.valueOf()
    ) ? {
      favcat,
    } : null;
    const paramsKeyword = keyword ? {
      f_search: keyword,
      f_apply: 'Search+Favorites',
    } : null;
    const paramsPage = page > 0 ? {
      page,
    } : null;

    return await axios.request({
      url: `${this.host}favorites.php`,
      params: {
        ...paramsFavCat,
        ...paramsKeyword,
        ...paramsPage,
      },
      data: {
        ddact: favcat === EhFavoriteCat.del.valueOf()
          ? 'delete'
          : `fav${favcat.toString()}`,
        apply: 'Apply',
        modifygids: gids,
      }
    })
  }

  public async torrents(url: string) {
    return await axios.request({
      url,
    })
  }

  public async archives(url: string) {
    return await axios.request({
      url,
    })
  }

  public async downloadArchive(
    gid: number,
    token: string,
    or: string,
    res: string,
  ) {
    return await axios.request({
      url: `${this.host}archiver.php`,
      params: {
        gid,
        token,
        or,
      },
      data: {
        hathdl_xres: res,
      }
    })
  }

  public async profile() {
    return await axios.request({
      url: 'https://forums.e-hentai.org/',
    })
  }

  public async profileInfo(url: string) {
    return await axios.request({
      url,
    })
  }

  public async voteComment(
    apiUid: number,
    apiKey: string,
    gid: number,
    token: string,
    commentId: number,
    commentVote: number,
  ) {
    return await axios.request({
      url: this.api,
      method: 'post',
      data: {
        method: 'votecomment',
        apiuid: apiUid,
        apikey: apiKey,
        gid,
        token,
        comment_id: commentId,
        comment_vote: commentVote,
      },
    })
  }

  public async galleryPage(
    pToken: string,
    gid: number,
    index: number,
    skipHathKey: string = null,
  ) {
    const paramsSkipHathKey = skipHathKey ? {
      nl: skipHathKey,
    } : null;
    return await axios.request({
      url: `${this.host}s/${pToken}/${gid}-${index + 1}`,
      params: {
        ...paramsSkipHathKey,
      },
    })
  }

  // public async gallerysByImageSearch() {
  //   await axios.request({
  //     url: this.domain === EhDomain.ehentai
  //       ? 'https://upload.e-hentai.org/image_lookup.php'
  //       : this.domain === EhDomain.exhentai
  //         ? 'https://exhentai.org/upload/image_lookup.php'
  //         : null,
  //   }) 
  // }

}

export enum EhDomain {
  exhentai = 'exhentai.org',
  ehentai = 'e-hentai.org',
}

export enum EhFavoriteCat {
  del = 'favdel',
  all = -1,
  local = -2,
  storage1 = 0,
  storage2 = 1,
  storage3 = 2,
  storage4 = 3,
  storage5 = 4,
  storage6 = 5,
  storage7 = 6,
  storage8 = 7,
  storage9 = 8,
  storage10 = 9,
}

export interface Category {
  doujinshi: boolean
  manga: boolean
  artistcg: boolean
  gamecg: boolean
  western: boolean
  non_h: boolean
  imageset: boolean
  cosplay: boolean
  asianporn: boolean
  misc: boolean
}

export interface AdvanceSearch {
  sname: boolean
  stags: boolean
  sdesc: boolean
  storr: boolean
  sto: boolean
  sdt1: boolean
  sdt2: boolean
  sh: boolean
}

export default EhClient;