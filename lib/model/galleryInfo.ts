interface GalleryInfo {
  gid: number;
  token: string;
  title: string;
  titleJpn: string;
  thumb: string;
  category: number;
  posted: string;
  uploader: string;
  rating: string;
  simpleTags: string[];
  thumbWidth: number;
  thumbHeight: number;
  spanSize: number;
  spanIndex: number;
  spanGroupIndex: number;
}